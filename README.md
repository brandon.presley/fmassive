# fmassive

[Massive](https://gitea.presley.nz/brandon.presley/Massive) has several
performance problems which get on my nerves. Also I don't like how many
breaking changes React Native made so I'm giving Flutter a try.
Eventually I might replace that React Native codebase with this one
and point the f-droid repository here, since we aim to implement the
same app just in Flutter.

## Getting Started

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
