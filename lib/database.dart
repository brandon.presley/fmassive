import 'dart:io';

import 'package:fmassive/gym_sets.dart';
import 'package:fmassive/main.dart';
import 'package:fmassive/plans.dart';
import 'package:moor/ffi.dart';
import 'package:moor/moor.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'settings.dart';

part 'database.g.dart';

@UseMoor(tables: [Settings, GymSets, Plans])
class MyDatabase extends _$MyDatabase {
  MyDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(
        onCreate: (Migrator m) async {
          print('Creating...');
          await m.createAll();
          var data = await (db.select(db.settings)..limit(1)).get();
          if (data.isEmpty) await db.into(db.settings).insert(defaultSettings);
        },
        onUpgrade: (Migrator m, int from, int to) async {},
      );
}

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getDatabasesPath();
    final file = File(join(dbFolder, 'massive.db'));
    return VmDatabase(file, logStatements: true);
  });
}
