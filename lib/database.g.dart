// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: type=lint
class Setting extends DataClass implements Insertable<Setting> {
  final bool alarm;
  final bool? backup;
  final String? darkColor;
  final String? date;
  final bool? images;
  final String? lightColor;
  final bool? noSound;
  final bool? notify;
  final bool? showDate;
  final bool? showSets;
  final bool? showUnit;
  final String? sound;
  final bool? steps;
  final String? theme;
  final bool vibrate;
  Setting(
      {required this.alarm,
      this.backup,
      this.darkColor,
      this.date,
      this.images,
      this.lightColor,
      this.noSound,
      this.notify,
      this.showDate,
      this.showSets,
      this.showUnit,
      this.sound,
      this.steps,
      this.theme,
      required this.vibrate});
  factory Setting.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Setting(
      alarm: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}alarm'])!,
      backup: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}backup']),
      darkColor: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}dark_color']),
      date: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}date']),
      images: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}images']),
      lightColor: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}light_color']),
      noSound: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}no_sound']),
      notify: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}notify']),
      showDate: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}show_date']),
      showSets: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}show_sets']),
      showUnit: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}show_unit']),
      sound: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}sound']),
      steps: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}steps']),
      theme: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}theme']),
      vibrate: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}vibrate'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['alarm'] = Variable<bool>(alarm);
    if (!nullToAbsent || backup != null) {
      map['backup'] = Variable<bool?>(backup);
    }
    if (!nullToAbsent || darkColor != null) {
      map['dark_color'] = Variable<String?>(darkColor);
    }
    if (!nullToAbsent || date != null) {
      map['date'] = Variable<String?>(date);
    }
    if (!nullToAbsent || images != null) {
      map['images'] = Variable<bool?>(images);
    }
    if (!nullToAbsent || lightColor != null) {
      map['light_color'] = Variable<String?>(lightColor);
    }
    if (!nullToAbsent || noSound != null) {
      map['no_sound'] = Variable<bool?>(noSound);
    }
    if (!nullToAbsent || notify != null) {
      map['notify'] = Variable<bool?>(notify);
    }
    if (!nullToAbsent || showDate != null) {
      map['show_date'] = Variable<bool?>(showDate);
    }
    if (!nullToAbsent || showSets != null) {
      map['show_sets'] = Variable<bool?>(showSets);
    }
    if (!nullToAbsent || showUnit != null) {
      map['show_unit'] = Variable<bool?>(showUnit);
    }
    if (!nullToAbsent || sound != null) {
      map['sound'] = Variable<String?>(sound);
    }
    if (!nullToAbsent || steps != null) {
      map['steps'] = Variable<bool?>(steps);
    }
    if (!nullToAbsent || theme != null) {
      map['theme'] = Variable<String?>(theme);
    }
    map['vibrate'] = Variable<bool>(vibrate);
    return map;
  }

  SettingsCompanion toCompanion(bool nullToAbsent) {
    return SettingsCompanion(
      alarm: Value(alarm),
      backup:
          backup == null && nullToAbsent ? const Value.absent() : Value(backup),
      darkColor: darkColor == null && nullToAbsent
          ? const Value.absent()
          : Value(darkColor),
      date: date == null && nullToAbsent ? const Value.absent() : Value(date),
      images:
          images == null && nullToAbsent ? const Value.absent() : Value(images),
      lightColor: lightColor == null && nullToAbsent
          ? const Value.absent()
          : Value(lightColor),
      noSound: noSound == null && nullToAbsent
          ? const Value.absent()
          : Value(noSound),
      notify:
          notify == null && nullToAbsent ? const Value.absent() : Value(notify),
      showDate: showDate == null && nullToAbsent
          ? const Value.absent()
          : Value(showDate),
      showSets: showSets == null && nullToAbsent
          ? const Value.absent()
          : Value(showSets),
      showUnit: showUnit == null && nullToAbsent
          ? const Value.absent()
          : Value(showUnit),
      sound:
          sound == null && nullToAbsent ? const Value.absent() : Value(sound),
      steps:
          steps == null && nullToAbsent ? const Value.absent() : Value(steps),
      theme:
          theme == null && nullToAbsent ? const Value.absent() : Value(theme),
      vibrate: Value(vibrate),
    );
  }

  factory Setting.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Setting(
      alarm: serializer.fromJson<bool>(json['alarm']),
      backup: serializer.fromJson<bool?>(json['backup']),
      darkColor: serializer.fromJson<String?>(json['darkColor']),
      date: serializer.fromJson<String?>(json['date']),
      images: serializer.fromJson<bool?>(json['images']),
      lightColor: serializer.fromJson<String?>(json['lightColor']),
      noSound: serializer.fromJson<bool?>(json['noSound']),
      notify: serializer.fromJson<bool?>(json['notify']),
      showDate: serializer.fromJson<bool?>(json['showDate']),
      showSets: serializer.fromJson<bool?>(json['showSets']),
      showUnit: serializer.fromJson<bool?>(json['showUnit']),
      sound: serializer.fromJson<String?>(json['sound']),
      steps: serializer.fromJson<bool?>(json['steps']),
      theme: serializer.fromJson<String?>(json['theme']),
      vibrate: serializer.fromJson<bool>(json['vibrate']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'alarm': serializer.toJson<bool>(alarm),
      'backup': serializer.toJson<bool?>(backup),
      'darkColor': serializer.toJson<String?>(darkColor),
      'date': serializer.toJson<String?>(date),
      'images': serializer.toJson<bool?>(images),
      'lightColor': serializer.toJson<String?>(lightColor),
      'noSound': serializer.toJson<bool?>(noSound),
      'notify': serializer.toJson<bool?>(notify),
      'showDate': serializer.toJson<bool?>(showDate),
      'showSets': serializer.toJson<bool?>(showSets),
      'showUnit': serializer.toJson<bool?>(showUnit),
      'sound': serializer.toJson<String?>(sound),
      'steps': serializer.toJson<bool?>(steps),
      'theme': serializer.toJson<String?>(theme),
      'vibrate': serializer.toJson<bool>(vibrate),
    };
  }

  Setting copyWith(
          {bool? alarm,
          bool? backup,
          String? darkColor,
          String? date,
          bool? images,
          String? lightColor,
          bool? noSound,
          bool? notify,
          bool? showDate,
          bool? showSets,
          bool? showUnit,
          String? sound,
          bool? steps,
          String? theme,
          bool? vibrate}) =>
      Setting(
        alarm: alarm ?? this.alarm,
        backup: backup ?? this.backup,
        darkColor: darkColor ?? this.darkColor,
        date: date ?? this.date,
        images: images ?? this.images,
        lightColor: lightColor ?? this.lightColor,
        noSound: noSound ?? this.noSound,
        notify: notify ?? this.notify,
        showDate: showDate ?? this.showDate,
        showSets: showSets ?? this.showSets,
        showUnit: showUnit ?? this.showUnit,
        sound: sound ?? this.sound,
        steps: steps ?? this.steps,
        theme: theme ?? this.theme,
        vibrate: vibrate ?? this.vibrate,
      );
  @override
  String toString() {
    return (StringBuffer('Setting(')
          ..write('alarm: $alarm, ')
          ..write('backup: $backup, ')
          ..write('darkColor: $darkColor, ')
          ..write('date: $date, ')
          ..write('images: $images, ')
          ..write('lightColor: $lightColor, ')
          ..write('noSound: $noSound, ')
          ..write('notify: $notify, ')
          ..write('showDate: $showDate, ')
          ..write('showSets: $showSets, ')
          ..write('showUnit: $showUnit, ')
          ..write('sound: $sound, ')
          ..write('steps: $steps, ')
          ..write('theme: $theme, ')
          ..write('vibrate: $vibrate')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      alarm,
      backup,
      darkColor,
      date,
      images,
      lightColor,
      noSound,
      notify,
      showDate,
      showSets,
      showUnit,
      sound,
      steps,
      theme,
      vibrate);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Setting &&
          other.alarm == this.alarm &&
          other.backup == this.backup &&
          other.darkColor == this.darkColor &&
          other.date == this.date &&
          other.images == this.images &&
          other.lightColor == this.lightColor &&
          other.noSound == this.noSound &&
          other.notify == this.notify &&
          other.showDate == this.showDate &&
          other.showSets == this.showSets &&
          other.showUnit == this.showUnit &&
          other.sound == this.sound &&
          other.steps == this.steps &&
          other.theme == this.theme &&
          other.vibrate == this.vibrate);
}

class SettingsCompanion extends UpdateCompanion<Setting> {
  final Value<bool> alarm;
  final Value<bool?> backup;
  final Value<String?> darkColor;
  final Value<String?> date;
  final Value<bool?> images;
  final Value<String?> lightColor;
  final Value<bool?> noSound;
  final Value<bool?> notify;
  final Value<bool?> showDate;
  final Value<bool?> showSets;
  final Value<bool?> showUnit;
  final Value<String?> sound;
  final Value<bool?> steps;
  final Value<String?> theme;
  final Value<bool> vibrate;
  const SettingsCompanion({
    this.alarm = const Value.absent(),
    this.backup = const Value.absent(),
    this.darkColor = const Value.absent(),
    this.date = const Value.absent(),
    this.images = const Value.absent(),
    this.lightColor = const Value.absent(),
    this.noSound = const Value.absent(),
    this.notify = const Value.absent(),
    this.showDate = const Value.absent(),
    this.showSets = const Value.absent(),
    this.showUnit = const Value.absent(),
    this.sound = const Value.absent(),
    this.steps = const Value.absent(),
    this.theme = const Value.absent(),
    this.vibrate = const Value.absent(),
  });
  SettingsCompanion.insert({
    required bool alarm,
    this.backup = const Value.absent(),
    this.darkColor = const Value.absent(),
    this.date = const Value.absent(),
    this.images = const Value.absent(),
    this.lightColor = const Value.absent(),
    this.noSound = const Value.absent(),
    this.notify = const Value.absent(),
    this.showDate = const Value.absent(),
    this.showSets = const Value.absent(),
    this.showUnit = const Value.absent(),
    this.sound = const Value.absent(),
    this.steps = const Value.absent(),
    this.theme = const Value.absent(),
    required bool vibrate,
  })  : alarm = Value(alarm),
        vibrate = Value(vibrate);
  static Insertable<Setting> custom({
    Expression<bool>? alarm,
    Expression<bool?>? backup,
    Expression<String?>? darkColor,
    Expression<String?>? date,
    Expression<bool?>? images,
    Expression<String?>? lightColor,
    Expression<bool?>? noSound,
    Expression<bool?>? notify,
    Expression<bool?>? showDate,
    Expression<bool?>? showSets,
    Expression<bool?>? showUnit,
    Expression<String?>? sound,
    Expression<bool?>? steps,
    Expression<String?>? theme,
    Expression<bool>? vibrate,
  }) {
    return RawValuesInsertable({
      if (alarm != null) 'alarm': alarm,
      if (backup != null) 'backup': backup,
      if (darkColor != null) 'dark_color': darkColor,
      if (date != null) 'date': date,
      if (images != null) 'images': images,
      if (lightColor != null) 'light_color': lightColor,
      if (noSound != null) 'no_sound': noSound,
      if (notify != null) 'notify': notify,
      if (showDate != null) 'show_date': showDate,
      if (showSets != null) 'show_sets': showSets,
      if (showUnit != null) 'show_unit': showUnit,
      if (sound != null) 'sound': sound,
      if (steps != null) 'steps': steps,
      if (theme != null) 'theme': theme,
      if (vibrate != null) 'vibrate': vibrate,
    });
  }

  SettingsCompanion copyWith(
      {Value<bool>? alarm,
      Value<bool?>? backup,
      Value<String?>? darkColor,
      Value<String?>? date,
      Value<bool?>? images,
      Value<String?>? lightColor,
      Value<bool?>? noSound,
      Value<bool?>? notify,
      Value<bool?>? showDate,
      Value<bool?>? showSets,
      Value<bool?>? showUnit,
      Value<String?>? sound,
      Value<bool?>? steps,
      Value<String?>? theme,
      Value<bool>? vibrate}) {
    return SettingsCompanion(
      alarm: alarm ?? this.alarm,
      backup: backup ?? this.backup,
      darkColor: darkColor ?? this.darkColor,
      date: date ?? this.date,
      images: images ?? this.images,
      lightColor: lightColor ?? this.lightColor,
      noSound: noSound ?? this.noSound,
      notify: notify ?? this.notify,
      showDate: showDate ?? this.showDate,
      showSets: showSets ?? this.showSets,
      showUnit: showUnit ?? this.showUnit,
      sound: sound ?? this.sound,
      steps: steps ?? this.steps,
      theme: theme ?? this.theme,
      vibrate: vibrate ?? this.vibrate,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (alarm.present) {
      map['alarm'] = Variable<bool>(alarm.value);
    }
    if (backup.present) {
      map['backup'] = Variable<bool?>(backup.value);
    }
    if (darkColor.present) {
      map['dark_color'] = Variable<String?>(darkColor.value);
    }
    if (date.present) {
      map['date'] = Variable<String?>(date.value);
    }
    if (images.present) {
      map['images'] = Variable<bool?>(images.value);
    }
    if (lightColor.present) {
      map['light_color'] = Variable<String?>(lightColor.value);
    }
    if (noSound.present) {
      map['no_sound'] = Variable<bool?>(noSound.value);
    }
    if (notify.present) {
      map['notify'] = Variable<bool?>(notify.value);
    }
    if (showDate.present) {
      map['show_date'] = Variable<bool?>(showDate.value);
    }
    if (showSets.present) {
      map['show_sets'] = Variable<bool?>(showSets.value);
    }
    if (showUnit.present) {
      map['show_unit'] = Variable<bool?>(showUnit.value);
    }
    if (sound.present) {
      map['sound'] = Variable<String?>(sound.value);
    }
    if (steps.present) {
      map['steps'] = Variable<bool?>(steps.value);
    }
    if (theme.present) {
      map['theme'] = Variable<String?>(theme.value);
    }
    if (vibrate.present) {
      map['vibrate'] = Variable<bool>(vibrate.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SettingsCompanion(')
          ..write('alarm: $alarm, ')
          ..write('backup: $backup, ')
          ..write('darkColor: $darkColor, ')
          ..write('date: $date, ')
          ..write('images: $images, ')
          ..write('lightColor: $lightColor, ')
          ..write('noSound: $noSound, ')
          ..write('notify: $notify, ')
          ..write('showDate: $showDate, ')
          ..write('showSets: $showSets, ')
          ..write('showUnit: $showUnit, ')
          ..write('sound: $sound, ')
          ..write('steps: $steps, ')
          ..write('theme: $theme, ')
          ..write('vibrate: $vibrate')
          ..write(')'))
        .toString();
  }
}

class $SettingsTable extends Settings with TableInfo<$SettingsTable, Setting> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SettingsTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _alarmMeta = const VerificationMeta('alarm');
  @override
  late final GeneratedColumn<bool?> alarm = GeneratedColumn<bool?>(
      'alarm', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: true,
      defaultConstraints: 'CHECK (alarm IN (0, 1))');
  final VerificationMeta _backupMeta = const VerificationMeta('backup');
  @override
  late final GeneratedColumn<bool?> backup = GeneratedColumn<bool?>(
      'backup', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (backup IN (0, 1))');
  final VerificationMeta _darkColorMeta = const VerificationMeta('darkColor');
  @override
  late final GeneratedColumn<String?> darkColor = GeneratedColumn<String?>(
      'dark_color', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _dateMeta = const VerificationMeta('date');
  @override
  late final GeneratedColumn<String?> date = GeneratedColumn<String?>(
      'date', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _imagesMeta = const VerificationMeta('images');
  @override
  late final GeneratedColumn<bool?> images = GeneratedColumn<bool?>(
      'images', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (images IN (0, 1))');
  final VerificationMeta _lightColorMeta = const VerificationMeta('lightColor');
  @override
  late final GeneratedColumn<String?> lightColor = GeneratedColumn<String?>(
      'light_color', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _noSoundMeta = const VerificationMeta('noSound');
  @override
  late final GeneratedColumn<bool?> noSound = GeneratedColumn<bool?>(
      'no_sound', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (no_sound IN (0, 1))');
  final VerificationMeta _notifyMeta = const VerificationMeta('notify');
  @override
  late final GeneratedColumn<bool?> notify = GeneratedColumn<bool?>(
      'notify', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (notify IN (0, 1))');
  final VerificationMeta _showDateMeta = const VerificationMeta('showDate');
  @override
  late final GeneratedColumn<bool?> showDate = GeneratedColumn<bool?>(
      'show_date', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (show_date IN (0, 1))');
  final VerificationMeta _showSetsMeta = const VerificationMeta('showSets');
  @override
  late final GeneratedColumn<bool?> showSets = GeneratedColumn<bool?>(
      'show_sets', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (show_sets IN (0, 1))');
  final VerificationMeta _showUnitMeta = const VerificationMeta('showUnit');
  @override
  late final GeneratedColumn<bool?> showUnit = GeneratedColumn<bool?>(
      'show_unit', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (show_unit IN (0, 1))');
  final VerificationMeta _soundMeta = const VerificationMeta('sound');
  @override
  late final GeneratedColumn<String?> sound = GeneratedColumn<String?>(
      'sound', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _stepsMeta = const VerificationMeta('steps');
  @override
  late final GeneratedColumn<bool?> steps = GeneratedColumn<bool?>(
      'steps', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (steps IN (0, 1))');
  final VerificationMeta _themeMeta = const VerificationMeta('theme');
  @override
  late final GeneratedColumn<String?> theme = GeneratedColumn<String?>(
      'theme', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _vibrateMeta = const VerificationMeta('vibrate');
  @override
  late final GeneratedColumn<bool?> vibrate = GeneratedColumn<bool?>(
      'vibrate', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: true,
      defaultConstraints: 'CHECK (vibrate IN (0, 1))');
  @override
  List<GeneratedColumn> get $columns => [
        alarm,
        backup,
        darkColor,
        date,
        images,
        lightColor,
        noSound,
        notify,
        showDate,
        showSets,
        showUnit,
        sound,
        steps,
        theme,
        vibrate
      ];
  @override
  String get aliasedName => _alias ?? 'settings';
  @override
  String get actualTableName => 'settings';
  @override
  VerificationContext validateIntegrity(Insertable<Setting> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('alarm')) {
      context.handle(
          _alarmMeta, alarm.isAcceptableOrUnknown(data['alarm']!, _alarmMeta));
    } else if (isInserting) {
      context.missing(_alarmMeta);
    }
    if (data.containsKey('backup')) {
      context.handle(_backupMeta,
          backup.isAcceptableOrUnknown(data['backup']!, _backupMeta));
    }
    if (data.containsKey('dark_color')) {
      context.handle(_darkColorMeta,
          darkColor.isAcceptableOrUnknown(data['dark_color']!, _darkColorMeta));
    }
    if (data.containsKey('date')) {
      context.handle(
          _dateMeta, date.isAcceptableOrUnknown(data['date']!, _dateMeta));
    }
    if (data.containsKey('images')) {
      context.handle(_imagesMeta,
          images.isAcceptableOrUnknown(data['images']!, _imagesMeta));
    }
    if (data.containsKey('light_color')) {
      context.handle(
          _lightColorMeta,
          lightColor.isAcceptableOrUnknown(
              data['light_color']!, _lightColorMeta));
    }
    if (data.containsKey('no_sound')) {
      context.handle(_noSoundMeta,
          noSound.isAcceptableOrUnknown(data['no_sound']!, _noSoundMeta));
    }
    if (data.containsKey('notify')) {
      context.handle(_notifyMeta,
          notify.isAcceptableOrUnknown(data['notify']!, _notifyMeta));
    }
    if (data.containsKey('show_date')) {
      context.handle(_showDateMeta,
          showDate.isAcceptableOrUnknown(data['show_date']!, _showDateMeta));
    }
    if (data.containsKey('show_sets')) {
      context.handle(_showSetsMeta,
          showSets.isAcceptableOrUnknown(data['show_sets']!, _showSetsMeta));
    }
    if (data.containsKey('show_unit')) {
      context.handle(_showUnitMeta,
          showUnit.isAcceptableOrUnknown(data['show_unit']!, _showUnitMeta));
    }
    if (data.containsKey('sound')) {
      context.handle(
          _soundMeta, sound.isAcceptableOrUnknown(data['sound']!, _soundMeta));
    }
    if (data.containsKey('steps')) {
      context.handle(
          _stepsMeta, steps.isAcceptableOrUnknown(data['steps']!, _stepsMeta));
    }
    if (data.containsKey('theme')) {
      context.handle(
          _themeMeta, theme.isAcceptableOrUnknown(data['theme']!, _themeMeta));
    }
    if (data.containsKey('vibrate')) {
      context.handle(_vibrateMeta,
          vibrate.isAcceptableOrUnknown(data['vibrate']!, _vibrateMeta));
    } else if (isInserting) {
      context.missing(_vibrateMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Setting map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Setting.fromData(data, attachedDatabase,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $SettingsTable createAlias(String alias) {
    return $SettingsTable(attachedDatabase, alias);
  }
}

class GymSet extends DataClass implements Insertable<GymSet> {
  final int id;
  final String name;
  final int reps;
  final double weight;
  final int sets;
  final int minutes;
  final int seconds;
  final bool? hidden;
  final String created;
  final String unit;
  final String? image;
  final String? steps;
  GymSet(
      {required this.id,
      required this.name,
      required this.reps,
      required this.weight,
      required this.sets,
      required this.minutes,
      required this.seconds,
      this.hidden,
      required this.created,
      required this.unit,
      this.image,
      this.steps});
  factory GymSet.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return GymSet(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      reps: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}reps'])!,
      weight: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}weight'])!,
      sets: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}sets'])!,
      minutes: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}minutes'])!,
      seconds: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}seconds'])!,
      hidden: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}hidden']),
      created: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}created'])!,
      unit: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}unit'])!,
      image: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image']),
      steps: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}steps']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['reps'] = Variable<int>(reps);
    map['weight'] = Variable<double>(weight);
    map['sets'] = Variable<int>(sets);
    map['minutes'] = Variable<int>(minutes);
    map['seconds'] = Variable<int>(seconds);
    if (!nullToAbsent || hidden != null) {
      map['hidden'] = Variable<bool?>(hidden);
    }
    map['created'] = Variable<String>(created);
    map['unit'] = Variable<String>(unit);
    if (!nullToAbsent || image != null) {
      map['image'] = Variable<String?>(image);
    }
    if (!nullToAbsent || steps != null) {
      map['steps'] = Variable<String?>(steps);
    }
    return map;
  }

  GymSetsCompanion toCompanion(bool nullToAbsent) {
    return GymSetsCompanion(
      id: Value(id),
      name: Value(name),
      reps: Value(reps),
      weight: Value(weight),
      sets: Value(sets),
      minutes: Value(minutes),
      seconds: Value(seconds),
      hidden:
          hidden == null && nullToAbsent ? const Value.absent() : Value(hidden),
      created: Value(created),
      unit: Value(unit),
      image:
          image == null && nullToAbsent ? const Value.absent() : Value(image),
      steps:
          steps == null && nullToAbsent ? const Value.absent() : Value(steps),
    );
  }

  factory GymSet.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return GymSet(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      reps: serializer.fromJson<int>(json['reps']),
      weight: serializer.fromJson<double>(json['weight']),
      sets: serializer.fromJson<int>(json['sets']),
      minutes: serializer.fromJson<int>(json['minutes']),
      seconds: serializer.fromJson<int>(json['seconds']),
      hidden: serializer.fromJson<bool?>(json['hidden']),
      created: serializer.fromJson<String>(json['created']),
      unit: serializer.fromJson<String>(json['unit']),
      image: serializer.fromJson<String?>(json['image']),
      steps: serializer.fromJson<String?>(json['steps']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'reps': serializer.toJson<int>(reps),
      'weight': serializer.toJson<double>(weight),
      'sets': serializer.toJson<int>(sets),
      'minutes': serializer.toJson<int>(minutes),
      'seconds': serializer.toJson<int>(seconds),
      'hidden': serializer.toJson<bool?>(hidden),
      'created': serializer.toJson<String>(created),
      'unit': serializer.toJson<String>(unit),
      'image': serializer.toJson<String?>(image),
      'steps': serializer.toJson<String?>(steps),
    };
  }

  GymSet copyWith(
          {int? id,
          String? name,
          int? reps,
          double? weight,
          int? sets,
          int? minutes,
          int? seconds,
          bool? hidden,
          String? created,
          String? unit,
          String? image,
          String? steps}) =>
      GymSet(
        id: id ?? this.id,
        name: name ?? this.name,
        reps: reps ?? this.reps,
        weight: weight ?? this.weight,
        sets: sets ?? this.sets,
        minutes: minutes ?? this.minutes,
        seconds: seconds ?? this.seconds,
        hidden: hidden ?? this.hidden,
        created: created ?? this.created,
        unit: unit ?? this.unit,
        image: image ?? this.image,
        steps: steps ?? this.steps,
      );
  @override
  String toString() {
    return (StringBuffer('GymSet(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('reps: $reps, ')
          ..write('weight: $weight, ')
          ..write('sets: $sets, ')
          ..write('minutes: $minutes, ')
          ..write('seconds: $seconds, ')
          ..write('hidden: $hidden, ')
          ..write('created: $created, ')
          ..write('unit: $unit, ')
          ..write('image: $image, ')
          ..write('steps: $steps')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, reps, weight, sets, minutes,
      seconds, hidden, created, unit, image, steps);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is GymSet &&
          other.id == this.id &&
          other.name == this.name &&
          other.reps == this.reps &&
          other.weight == this.weight &&
          other.sets == this.sets &&
          other.minutes == this.minutes &&
          other.seconds == this.seconds &&
          other.hidden == this.hidden &&
          other.created == this.created &&
          other.unit == this.unit &&
          other.image == this.image &&
          other.steps == this.steps);
}

class GymSetsCompanion extends UpdateCompanion<GymSet> {
  final Value<int> id;
  final Value<String> name;
  final Value<int> reps;
  final Value<double> weight;
  final Value<int> sets;
  final Value<int> minutes;
  final Value<int> seconds;
  final Value<bool?> hidden;
  final Value<String> created;
  final Value<String> unit;
  final Value<String?> image;
  final Value<String?> steps;
  const GymSetsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.reps = const Value.absent(),
    this.weight = const Value.absent(),
    this.sets = const Value.absent(),
    this.minutes = const Value.absent(),
    this.seconds = const Value.absent(),
    this.hidden = const Value.absent(),
    this.created = const Value.absent(),
    this.unit = const Value.absent(),
    this.image = const Value.absent(),
    this.steps = const Value.absent(),
  });
  GymSetsCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    required int reps,
    required double weight,
    this.sets = const Value.absent(),
    this.minutes = const Value.absent(),
    this.seconds = const Value.absent(),
    this.hidden = const Value.absent(),
    required String created,
    this.unit = const Value.absent(),
    this.image = const Value.absent(),
    this.steps = const Value.absent(),
  })  : name = Value(name),
        reps = Value(reps),
        weight = Value(weight),
        created = Value(created);
  static Insertable<GymSet> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<int>? reps,
    Expression<double>? weight,
    Expression<int>? sets,
    Expression<int>? minutes,
    Expression<int>? seconds,
    Expression<bool?>? hidden,
    Expression<String>? created,
    Expression<String>? unit,
    Expression<String?>? image,
    Expression<String?>? steps,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (reps != null) 'reps': reps,
      if (weight != null) 'weight': weight,
      if (sets != null) 'sets': sets,
      if (minutes != null) 'minutes': minutes,
      if (seconds != null) 'seconds': seconds,
      if (hidden != null) 'hidden': hidden,
      if (created != null) 'created': created,
      if (unit != null) 'unit': unit,
      if (image != null) 'image': image,
      if (steps != null) 'steps': steps,
    });
  }

  GymSetsCompanion copyWith(
      {Value<int>? id,
      Value<String>? name,
      Value<int>? reps,
      Value<double>? weight,
      Value<int>? sets,
      Value<int>? minutes,
      Value<int>? seconds,
      Value<bool?>? hidden,
      Value<String>? created,
      Value<String>? unit,
      Value<String?>? image,
      Value<String?>? steps}) {
    return GymSetsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      reps: reps ?? this.reps,
      weight: weight ?? this.weight,
      sets: sets ?? this.sets,
      minutes: minutes ?? this.minutes,
      seconds: seconds ?? this.seconds,
      hidden: hidden ?? this.hidden,
      created: created ?? this.created,
      unit: unit ?? this.unit,
      image: image ?? this.image,
      steps: steps ?? this.steps,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (reps.present) {
      map['reps'] = Variable<int>(reps.value);
    }
    if (weight.present) {
      map['weight'] = Variable<double>(weight.value);
    }
    if (sets.present) {
      map['sets'] = Variable<int>(sets.value);
    }
    if (minutes.present) {
      map['minutes'] = Variable<int>(minutes.value);
    }
    if (seconds.present) {
      map['seconds'] = Variable<int>(seconds.value);
    }
    if (hidden.present) {
      map['hidden'] = Variable<bool?>(hidden.value);
    }
    if (created.present) {
      map['created'] = Variable<String>(created.value);
    }
    if (unit.present) {
      map['unit'] = Variable<String>(unit.value);
    }
    if (image.present) {
      map['image'] = Variable<String?>(image.value);
    }
    if (steps.present) {
      map['steps'] = Variable<String?>(steps.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('GymSetsCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('reps: $reps, ')
          ..write('weight: $weight, ')
          ..write('sets: $sets, ')
          ..write('minutes: $minutes, ')
          ..write('seconds: $seconds, ')
          ..write('hidden: $hidden, ')
          ..write('created: $created, ')
          ..write('unit: $unit, ')
          ..write('image: $image, ')
          ..write('steps: $steps')
          ..write(')'))
        .toString();
  }
}

class $GymSetsTable extends GymSets with TableInfo<$GymSetsTable, GymSet> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $GymSetsTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _repsMeta = const VerificationMeta('reps');
  @override
  late final GeneratedColumn<int?> reps = GeneratedColumn<int?>(
      'reps', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _weightMeta = const VerificationMeta('weight');
  @override
  late final GeneratedColumn<double?> weight = GeneratedColumn<double?>(
      'weight', aliasedName, false,
      type: const RealType(), requiredDuringInsert: true);
  final VerificationMeta _setsMeta = const VerificationMeta('sets');
  @override
  late final GeneratedColumn<int?> sets = GeneratedColumn<int?>(
      'sets', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(3));
  final VerificationMeta _minutesMeta = const VerificationMeta('minutes');
  @override
  late final GeneratedColumn<int?> minutes = GeneratedColumn<int?>(
      'minutes', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(3));
  final VerificationMeta _secondsMeta = const VerificationMeta('seconds');
  @override
  late final GeneratedColumn<int?> seconds = GeneratedColumn<int?>(
      'seconds', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(30));
  final VerificationMeta _hiddenMeta = const VerificationMeta('hidden');
  @override
  late final GeneratedColumn<bool?> hidden = GeneratedColumn<bool?>(
      'hidden', aliasedName, true,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (hidden IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _createdMeta = const VerificationMeta('created');
  @override
  late final GeneratedColumn<String?> created = GeneratedColumn<String?>(
      'created', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _unitMeta = const VerificationMeta('unit');
  @override
  late final GeneratedColumn<String?> unit = GeneratedColumn<String?>(
      'unit', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant('kg'));
  final VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String?> image = GeneratedColumn<String?>(
      'image', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _stepsMeta = const VerificationMeta('steps');
  @override
  late final GeneratedColumn<String?> steps = GeneratedColumn<String?>(
      'steps', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        name,
        reps,
        weight,
        sets,
        minutes,
        seconds,
        hidden,
        created,
        unit,
        image,
        steps
      ];
  @override
  String get aliasedName => _alias ?? 'gym_sets';
  @override
  String get actualTableName => 'gym_sets';
  @override
  VerificationContext validateIntegrity(Insertable<GymSet> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('reps')) {
      context.handle(
          _repsMeta, reps.isAcceptableOrUnknown(data['reps']!, _repsMeta));
    } else if (isInserting) {
      context.missing(_repsMeta);
    }
    if (data.containsKey('weight')) {
      context.handle(_weightMeta,
          weight.isAcceptableOrUnknown(data['weight']!, _weightMeta));
    } else if (isInserting) {
      context.missing(_weightMeta);
    }
    if (data.containsKey('sets')) {
      context.handle(
          _setsMeta, sets.isAcceptableOrUnknown(data['sets']!, _setsMeta));
    }
    if (data.containsKey('minutes')) {
      context.handle(_minutesMeta,
          minutes.isAcceptableOrUnknown(data['minutes']!, _minutesMeta));
    }
    if (data.containsKey('seconds')) {
      context.handle(_secondsMeta,
          seconds.isAcceptableOrUnknown(data['seconds']!, _secondsMeta));
    }
    if (data.containsKey('hidden')) {
      context.handle(_hiddenMeta,
          hidden.isAcceptableOrUnknown(data['hidden']!, _hiddenMeta));
    }
    if (data.containsKey('created')) {
      context.handle(_createdMeta,
          created.isAcceptableOrUnknown(data['created']!, _createdMeta));
    } else if (isInserting) {
      context.missing(_createdMeta);
    }
    if (data.containsKey('unit')) {
      context.handle(
          _unitMeta, unit.isAcceptableOrUnknown(data['unit']!, _unitMeta));
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    }
    if (data.containsKey('steps')) {
      context.handle(
          _stepsMeta, steps.isAcceptableOrUnknown(data['steps']!, _stepsMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  GymSet map(Map<String, dynamic> data, {String? tablePrefix}) {
    return GymSet.fromData(data, attachedDatabase,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $GymSetsTable createAlias(String alias) {
    return $GymSetsTable(attachedDatabase, alias);
  }
}

class Plan extends DataClass implements Insertable<Plan> {
  final int id;
  final String days;
  final String exercises;
  Plan({required this.id, required this.days, required this.exercises});
  factory Plan.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Plan(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      days: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}days'])!,
      exercises: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}exercises'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['days'] = Variable<String>(days);
    map['exercises'] = Variable<String>(exercises);
    return map;
  }

  PlansCompanion toCompanion(bool nullToAbsent) {
    return PlansCompanion(
      id: Value(id),
      days: Value(days),
      exercises: Value(exercises),
    );
  }

  factory Plan.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Plan(
      id: serializer.fromJson<int>(json['id']),
      days: serializer.fromJson<String>(json['days']),
      exercises: serializer.fromJson<String>(json['exercises']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'days': serializer.toJson<String>(days),
      'exercises': serializer.toJson<String>(exercises),
    };
  }

  Plan copyWith({int? id, String? days, String? exercises}) => Plan(
        id: id ?? this.id,
        days: days ?? this.days,
        exercises: exercises ?? this.exercises,
      );
  @override
  String toString() {
    return (StringBuffer('Plan(')
          ..write('id: $id, ')
          ..write('days: $days, ')
          ..write('exercises: $exercises')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, days, exercises);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Plan &&
          other.id == this.id &&
          other.days == this.days &&
          other.exercises == this.exercises);
}

class PlansCompanion extends UpdateCompanion<Plan> {
  final Value<int> id;
  final Value<String> days;
  final Value<String> exercises;
  const PlansCompanion({
    this.id = const Value.absent(),
    this.days = const Value.absent(),
    this.exercises = const Value.absent(),
  });
  PlansCompanion.insert({
    this.id = const Value.absent(),
    required String days,
    required String exercises,
  })  : days = Value(days),
        exercises = Value(exercises);
  static Insertable<Plan> custom({
    Expression<int>? id,
    Expression<String>? days,
    Expression<String>? exercises,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (days != null) 'days': days,
      if (exercises != null) 'exercises': exercises,
    });
  }

  PlansCompanion copyWith(
      {Value<int>? id, Value<String>? days, Value<String>? exercises}) {
    return PlansCompanion(
      id: id ?? this.id,
      days: days ?? this.days,
      exercises: exercises ?? this.exercises,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (days.present) {
      map['days'] = Variable<String>(days.value);
    }
    if (exercises.present) {
      map['exercises'] = Variable<String>(exercises.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PlansCompanion(')
          ..write('id: $id, ')
          ..write('days: $days, ')
          ..write('exercises: $exercises')
          ..write(')'))
        .toString();
  }
}

class $PlansTable extends Plans with TableInfo<$PlansTable, Plan> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PlansTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _daysMeta = const VerificationMeta('days');
  @override
  late final GeneratedColumn<String?> days = GeneratedColumn<String?>(
      'days', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _exercisesMeta = const VerificationMeta('exercises');
  @override
  late final GeneratedColumn<String?> exercises = GeneratedColumn<String?>(
      'exercises', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, days, exercises];
  @override
  String get aliasedName => _alias ?? 'plans';
  @override
  String get actualTableName => 'plans';
  @override
  VerificationContext validateIntegrity(Insertable<Plan> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('days')) {
      context.handle(
          _daysMeta, days.isAcceptableOrUnknown(data['days']!, _daysMeta));
    } else if (isInserting) {
      context.missing(_daysMeta);
    }
    if (data.containsKey('exercises')) {
      context.handle(_exercisesMeta,
          exercises.isAcceptableOrUnknown(data['exercises']!, _exercisesMeta));
    } else if (isInserting) {
      context.missing(_exercisesMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Plan map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Plan.fromData(data, attachedDatabase,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $PlansTable createAlias(String alias) {
    return $PlansTable(attachedDatabase, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $SettingsTable settings = $SettingsTable(this);
  late final $GymSetsTable gymSets = $GymSetsTable(this);
  late final $PlansTable plans = $PlansTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [settings, gymSets, plans];
}
