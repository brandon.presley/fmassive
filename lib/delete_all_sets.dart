import 'package:flutter/material.dart';
import 'package:fmassive/main.dart';
import 'package:moor/moor.dart';

class DeleteAllSets extends StatelessWidget {
  const DeleteAllSets({
    super.key,
    required this.mounted,
  });

  final bool mounted;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        child: const Text("Delete all sets"),
        onPressed: () async {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: const Text("Delete all sets"),
                  content: const Text(
                      "This will irreversibly destroy all your gym set data. Are you sure?",),
                  actions: <Widget>[
                    ElevatedButton(
                      child: const Text('Cancel'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    ElevatedButton(
                      child: const Text('Delete'),
                      onPressed: () async {
                        await db.gymSets.delete().go();
                        if (!mounted) return;
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Deleted all sets')),);
                        final navigator = Navigator.of(context);
                        navigator.pop();
                      },
                    ),
                  ],
                );
              },);
        },
      ),
    );
  }
}
