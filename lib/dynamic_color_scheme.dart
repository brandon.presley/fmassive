import 'package:flutter/material.dart';

class DynamicColorScheme extends StatefulWidget {
  final Color Function(Brightness brightness) data;
  final Widget Function(BuildContext context, ColorScheme colorScheme) builder;

  const DynamicColorScheme({
    required this.data,
    required this.builder,
    super.key,
  });

  @override
  createState() => _DynamicColorSchemeState();
}

class _DynamicColorSchemeState extends State<DynamicColorScheme> {
  late final ValueNotifier<Brightness> brightness;

  @override
  void initState() {
    super.initState();
    brightness = ValueNotifier(MediaQuery.of(context).platformBrightness);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    brightness.value = MediaQuery.of(context).platformBrightness;
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Brightness>(
      valueListenable: brightness,
      builder: (context, platformBrightness, child) {
        final colorScheme = ColorScheme.fromSwatch(
          brightness: platformBrightness,
          primarySwatch: Colors.blue,
          accentColor: Colors.blueAccent,
        ).copyWith(
          secondary: Colors.red,
        );
        final color = widget.data(platformBrightness);
        return widget.builder(
            context,
            colorScheme.copyWith(
              primary: color,
            ),);
      },
    );
  }
}
