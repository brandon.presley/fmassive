import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as material;
import 'package:fmassive/constants.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/main.dart';
import 'package:moor_flutter/moor_flutter.dart';

class EditPlanPage extends StatefulWidget {
  final PlansCompanion plan;

  const EditPlanPage({required this.plan, super.key});

  @override
  createState() => _EditPlanPageState();
}

class _EditPlanPageState extends State<EditPlanPage> {
  List<String?> names = [];
  List<bool>? daySelections;
  List<bool>? exerciseSelections;

  Future<List<String?>> getDistinctNames() async {
    final names = await (db.gymSets.selectOnly(distinct: true)
          ..addColumns([db.gymSets.name]))
        .get();
    return names.map((name) => name.read(db.gymSets.name)).toList();
  }

  @override
  void initState() {
    super.initState();
    final dayList = widget.plan.days.value.split(',');
    daySelections = weekdayNames.map((day) => dayList.contains(day)).toList();

    getDistinctNames().then((value) {
      setState(() {
        names = value;
        final exercises = widget.plan.exercises.value.split(',');
        exerciseSelections =
            names.map((name) => exercises.contains(name)).toList();
      });
    });
  }

  @override
  dispose() {
    super.dispose();
  }

  Future<bool?> _showConfirmationDialog(BuildContext context) {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Confirm Delete'),
          content: const Text('Are you sure you want to delete this plan?'),
          actions: <Widget>[
            ElevatedButton(
              child: const Text('Yes'),
              onPressed: () {
                Navigator.pop(context, true); // showDialog() returns true
              },
            ),
            ElevatedButton(
              child: const Text('No'),
              onPressed: () {
                Navigator.pop(context, false); // showDialog() returns false
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> actions = [];
    if (widget.plan.id.present)
      actions.add(
        IconButton(
          onPressed: () async {
            bool? confirm = await _showConfirmationDialog(context);
            if (!confirm!) return;
            await db.plans.deleteOne(widget.plan);
            if (!mounted) return;
            Navigator.pop(context);
          },
          icon: const Icon(Icons.delete),
        ),
      );

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(title: const Text('Edit Plan'), actions: actions),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: material.Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: getChildren,
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            final days = [];
            for (int i = 0; i < daySelections!.length; i++) {
              if (daySelections![i]) days.add(weekdayNames[i]);
            }
            if (days.isEmpty) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Select days first')),
              );
              return;
            }

            final exercises = [];
            for (int i = 0; i < exerciseSelections!.length; i++) {
              if (exerciseSelections![i]) exercises.add(names[i]);
            }
            if (exercises.isEmpty) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Select exercises first')),
              );
              return;
            }

            var newPlan = widget.plan.copyWith(
              days: Value(days.join(',')),
              exercises: Value(exercises.join(',')),
            );

            if (widget.plan.id.present)
              await db.update(db.plans).replace(newPlan);
            else {
              final id = await db.into(db.plans).insert(newPlan);
              newPlan = newPlan.copyWith(id: Value(id));
            }

            if (!mounted) return;
            Navigator.pop(context);
          },
          child: const Icon(Icons.check),
        ),
      ),
    );
  }

  List<Widget> get getChildren {
    final List<Widget> children = [
      Text('Days', style: Theme.of(context).textTheme.headlineSmall),
    ];

    final days = List.generate(7, (index) {
      return SwitchListTile(
        title: Text(weekdayNames[index]),
        value: daySelections![index],
        onChanged: (value) {
          setState(() {
            daySelections![index] = value;
          });
        },
      );
    });

    final exercises = List.generate(names.length, (index) {
      return SwitchListTile(
        title: Text(names[index] ?? ''),
        value: exerciseSelections![index],
        onChanged: (value) {
          setState(() {
            exerciseSelections![index] = value;
          });
        },
      );
    });

    children.addAll(days);
    children.add(
      Text('Exercises', style: Theme.of(context).textTheme.headlineSmall),
    );
    children.addAll(exercises);

    return [
      Expanded(
        child: ListView(
          children: children,
        ),
      ),
    ];
  }
}
