import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter/services.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/main.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:permission_handler/permission_handler.dart';

class EditGymSetPage extends StatefulWidget {
  final GymSetsCompanion gymSet;

  const EditGymSetPage({required this.gymSet, super.key});

  @override
  createState() => _EditGymSetPageState();
}

class _EditGymSetPageState extends State<EditGymSetPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _repsController = TextEditingController();
  final TextEditingController _weightController = TextEditingController();
  late GymSetsCompanion gymSet;
  final nameNode = FocusNode();
  final repsNode = FocusNode();

  @override
  void initState() {
    super.initState();
    gymSet = widget.gymSet;
    print(gymSet);
    _nameController.text = gymSet.name.value;
    _repsController.text = gymSet.reps.value.toString();
    _weightController.text = gymSet.weight.value.toString();
    if (gymSet.id.present) {
      repsNode.requestFocus();
      _repsController.selection = TextSelection(
          baseOffset: 0, extentOffset: _repsController.text.length,);
    } else
      nameNode.requestFocus();
  }

  @override
  dispose() {
    nameNode.dispose();
    repsNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> actions = [];
    if (widget.gymSet.id.present)
      actions.add(IconButton(
          onPressed: () async {
            await db.gymSets.deleteOne(widget.gymSet);
            if (!mounted) return;
            Navigator.pop(context);
          },
          icon: const Icon(Icons.delete),),);

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(title: const Text('Edit Gym Set'), actions: actions),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: material.Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              controller: _nameController,
              focusNode: nameNode,
              decoration: const InputDecoration(labelText: 'Name'),
              onTap: () {
                _nameController.selection = TextSelection(
                    baseOffset: 0, extentOffset: _nameController.text.length,);
              },
              onChanged: (value) {
                setState(() {
                  gymSet = gymSet.copyWith(name: Value(value));
                });
              },
            ),
            TextFormField(
              controller: _repsController,
              focusNode: repsNode,
              onTap: () {
                _repsController.selection = TextSelection(
                    baseOffset: 0, extentOffset: _repsController.text.length,);
              },
              decoration: const InputDecoration(labelText: 'Reps'),
              keyboardType: TextInputType.number,
              onChanged: (value) {
                setState(() {
                  gymSet =
                      gymSet.copyWith(reps: Value(int.tryParse(value) ?? 0));
                });
              },
            ),
            TextFormField(
              controller: _weightController,
              decoration: const InputDecoration(labelText: 'Weight (kg)'),
              keyboardType: TextInputType.number,
              onTap: () {
                _weightController.selection = TextSelection(
                    baseOffset: 0, extentOffset: _weightController.text.length,);
              },
              onChanged: (value) {
                setState(() {
                  gymSet = gymSet.copyWith(
                      weight: Value(double.tryParse(value) ?? 0),);
                });
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (_nameController.text.isEmpty) {
            ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Please enter a name')),);
            nameNode.requestFocus();
            return;
          }
          if (gymSet.id.present)
            await db.update(db.gymSets).replace(gymSet);
          else {
            await Permission.notification.request();
            final newSet = gymSet.copyWith(
                created: Value(DateTime.now().toIso8601String()),);
            await db.into(db.gymSets).insert(newSet);
            const platform = MethodChannel('com.massive/android');
            platform.invokeMethod('timer', [3000]);
          }
          if (!mounted) return;
          Navigator.pop(context);
        },
        child: const Icon(Icons.check),
      ),
    ),);
  }
}
