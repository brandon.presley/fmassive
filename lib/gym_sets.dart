import 'package:moor_flutter/moor_flutter.dart';

class GymSets extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  IntColumn get reps => integer()();
  RealColumn get weight => real()();
  IntColumn get sets => integer().withDefault(const Constant(3))();
  IntColumn get minutes => integer().withDefault(const Constant(3))();
  IntColumn get seconds => integer().withDefault(const Constant(30))();
  BoolColumn get hidden =>
      boolean().nullable().withDefault(const Constant(false))();
  TextColumn get created => text()();
  TextColumn get unit => text().withDefault(const Constant('kg'))();
  TextColumn get image => text().nullable()();
  TextColumn get steps => text().nullable()();
}
