import 'package:flutter/material.dart';
import 'package:fmassive/best_page.dart';
import 'package:fmassive/plans_page.dart';
import 'package:fmassive/set_list.dart';
import 'package:fmassive/settings_page.dart';
import 'package:fmassive/timer_page.dart';
import 'package:fmassive/workouts_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  bool showSearch = false;
  int selected = 0;
  String search = '';
  final focusNode = FocusNode();

  final List<Map<String, dynamic>> routes = [
    {'title': 'Home', 'icon': Icons.home},
    {'title': 'Plans', 'icon': Icons.calendar_today},
    {'title': 'Best', 'icon': Icons.star},
    {'title': 'Workouts', 'icon': Icons.fitness_center},
    {'title': 'Timer', 'icon': Icons.timer},
    {'title': 'Settings', 'icon': Icons.settings},
  ];

  void toggleSearch() {
    setState(() {
      if (showSearch) search = '';
      showSearch = !showSearch;
    });
    focusNode.requestFocus();
  }

  Widget getBody() {
    switch (routes[selected]['title']) {
      case 'Settings':
        return SettingsPage(search: search);
      case 'Timer':
        return const TimerPage();
      case 'Workouts':
        return const WorkoutsPage();
      case 'Best':
        return const BestPage();
      case 'Plans':
        return PlansPage(search: search);
      default:
        return SetList(search: search);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      drawerEdgeDragWidth: 200.0,
      drawer: Drawer(
        child: ListView.builder(
          itemCount: routes.length,
          itemBuilder: (context, index) {
            return ListTile(
              selected: selected == index,
              leading: Icon(routes[index]['icon']),
              title: Text(routes[index]['title']),
              onTap: () {
                setState(() {
                  selected = index;
                });
                Navigator.pop(context);
              },
            );
          },
        ),
      ),
      appBar: AppBar(
        title: showSearch
            ? TextField(
                focusNode: focusNode,
                cursorColor: Colors.white,
                onChanged: (String value) {
                  setState(() {
                    search = value;
                  });
                },
                decoration: const InputDecoration(
                  hintText: 'Search...',
                  border: InputBorder.none,
                  hintStyle: TextStyle(color: Colors.white),
                ),
              )
            : Text(routes[selected]['title']),
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: toggleSearch,
          ),
        ],
      ),
      body: getBody(),
    ),);
  }
}
