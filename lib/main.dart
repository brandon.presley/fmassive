import 'package:flutter/material.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/home_page.dart';

MyDatabase db = MyDatabase();

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Massive',
      themeMode: ThemeMode.system,
      home: const HomePage(),
      darkTheme: ThemeData.dark(),
      theme: ThemeData.light(),
      navigatorKey: navigatorKey,
    );
  }
}
