import 'package:flutter/material.dart';

class MassiveDrawer extends StatefulWidget {
  final Widget body;
  final PreferredSizeWidget appBar;

  const MassiveDrawer({super.key, required this.body, required this.appBar});

  @override
  createState() => _MassiveDrawer();
}

class _MassiveDrawer extends State<MassiveDrawer> {
  final List<Map<String, dynamic>> routes = [
    {'title': 'Home', 'icon': Icons.home},
    {'title': 'Plans', 'icon': Icons.calendar_today},
    {'title': 'Best', 'icon': Icons.star},
    {'title': 'Workouts', 'icon': Icons.fitness_center},
    {'title': 'Timer', 'icon': Icons.timer},
    {'title': 'Settings', 'icon': Icons.settings},
  ];
  int selected = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widget.body,
      appBar: widget.appBar,
    );
  }
}
