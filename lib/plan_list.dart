import 'package:flutter/material.dart';
import 'package:fmassive/constants.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/plan_tile.dart';

class PlanList extends StatelessWidget {
  const PlanList({
    super.key,
    required this.plans,
  });

  final List<Plan> plans;

  @override
  Widget build(BuildContext context) {
    final weekday = weekdayNames[DateTime.now().weekday - 1];

    return ListView.builder(
      itemCount: plans.length,
      itemBuilder: (context, index) {
        return PlanTile(plan: plans[index], weekday: weekday);
      },
    );
  }
}
