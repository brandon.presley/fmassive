import 'package:flutter/material.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/edit_plan.dart';
import 'package:fmassive/main.dart';
import 'package:fmassive/start_plan.dart';
import 'package:moor_flutter/moor_flutter.dart';

class PlanTile extends StatelessWidget {
  PlanTile({
    super.key,
    required this.plan,
    required this.weekday,
  });

  final Plan plan;
  final String weekday;
  final tapPosition = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return MenuAnchor(
      menuChildren: [
        MenuItemButton(
          child: const Text("Edit"),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    EditPlanPage(plan: plan.toCompanion(false)),
              ),
            );
          },
        ),
        MenuItemButton(
          onPressed: () async {
            await db.plans.deleteOne(plan);
          },
          child: const Text("Delete"),
        ),
      ],
      builder: (context, controller, child) {
        return ListTile(
          title: Text(
            plan.days.replaceAll(',', ', '),
            style: TextStyle(
              fontWeight: plan.days.contains(weekday) ? FontWeight.bold : null,
              decoration:
                  plan.days.contains(weekday) ? TextDecoration.underline : null,
            ),
          ),
          subtitle: Text(plan.exercises),
          onLongPress: () {
            controller.open();
          },
          onTap: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => StartPlan(plan: plan.toCompanion(false)),
              ),
            );
          },
        );
      },
    );
  }
}
