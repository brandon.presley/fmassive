import 'package:moor/moor.dart';

class Plans extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get days => text()();
  TextColumn get exercises => text()();
}
