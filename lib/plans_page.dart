import 'package:flutter/material.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/edit_plan.dart';
import 'package:fmassive/main.dart';
import 'package:fmassive/plan_list.dart';
import 'package:moor/moor.dart';

class PlansPage extends StatelessWidget {
  const PlansPage({super.key, required this.search});

  final String search;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _PlansPage(search: search),
      ),
    );
  }
}

class _PlansPage extends StatefulWidget {
  const _PlansPage({required this.search});

  final String search;

  @override
  createState() => _PlansPageState();
}

class _PlansPageState extends State<_PlansPage> {
  bool showSearch = false;
  late Stream<List<Plan>> stream;

  @override
  initState() {
    super.initState();
    setStream();
  }

  void setStream() {
    stream = (db.select(db.plans)
          ..where((gymSet) => gymSet.days.contains(widget.search))
          ..limit(10, offset: 0))
        .watch();
  }

  @override
  didUpdateWidget(covariant _PlansPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    setStream();
  }

  void toggleSearch() {
    setState(() {
      showSearch = !showSearch;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder<List<Plan>>(
            stream: stream,
            builder: (context, snapshot) {
              final plans = snapshot.data;

              if (snapshot.hasError)
                return Center(
                  child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(
                        'Error: ${snapshot.error}',
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),),
                );

              if (plans == null) return Container();

              return PlanList(plans: plans);
            },),
        floatingActionButton: FloatingActionButton(
            onPressed: () async {
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const EditPlanPage(
                      plan: PlansCompanion(
                          days: Value(''), exercises: Value(''),),),
                ),
              );
            },
            child: const Icon(Icons.add),),);
  }
}
