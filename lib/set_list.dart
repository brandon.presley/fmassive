import 'package:flutter/material.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/edit_set.dart';
import 'package:fmassive/main.dart';
import 'package:fmassive/set_tile.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:moor_flutter/moor_flutter.dart';

class SetList extends StatefulWidget {
  final String search;

  const SetList({super.key, required this.search});

  @override
  createState() => _SetList();
}

class _SetList extends State<SetList> {
  bool showSearch = false;
  final PagingController<int, GymSet> pagingController =
      PagingController(firstPageKey: 0);

  @override
  initState() {
    super.initState();
    pagingController.addPageRequestListener((pageKey) {
      fetch(pageKey);
    });
  }

  @override
  didUpdateWidget(covariant SetList oldWidget) {
    super.didUpdateWidget(oldWidget);
    pagingController.refresh();
  }

  Future<void> fetch(int pageKey) async {
    final gymSets = await (db.select(db.gymSets)
          ..where((gymSet) => gymSet.name.contains(widget.search))
          ..orderBy([
            (u) => OrderingTerm(expression: u.created, mode: OrderingMode.desc),
          ])
          ..limit(10, offset: pageKey * 10))
        .get();

    final isLastPage = gymSets.length < 10;

    if (isLastPage) {
      pagingController.appendLastPage(gymSets);
    } else {
      final nextPageKey = pageKey + 1;
      pagingController.appendPage(gymSets, nextPageKey);
    }
  }

  void toggleSearch() {
    setState(() {
      showSearch = !showSearch;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PagedListView<int, GymSet>(
        pagingController: pagingController,
        builderDelegate: PagedChildBuilderDelegate<GymSet>(
          itemBuilder: (context, gymSet, index) => SetTile(
            pagingController: pagingController,
            gymSet: gymSet,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const EditGymSetPage(
                gymSet: GymSetsCompanion(
                  name: Value(''),
                  reps: Value(0),
                  weight: Value(0),
                  image: Value(''),
                ),
              ),
            ),
          );
          pagingController.refresh();
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
