import 'package:flutter/material.dart';
import 'package:fmassive/database.dart';
import 'package:fmassive/edit_set.dart';
import 'package:fmassive/main.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:moor/moor.dart';

class SetTile extends StatelessWidget {
  const SetTile({
    super.key,
    required this.pagingController,
    required this.gymSet,
  });

  final PagingController<int, GymSet> pagingController;
  final GymSet gymSet;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(gymSet.name),
      subtitle: Text(DateFormat("yyyy-MM-dd HH:mm")
          .format(DateTime.parse(gymSet.created)),),
      trailing: Text("${gymSet.reps} x ${gymSet.weight}kg"),
      onTap: () async {
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditGymSetPage(
              gymSet: gymSet.toCompanion(false),
            ),
          ),
        );
        pagingController.refresh();
      },
      onLongPress: () => showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Delete set'),
            content: Text(
                'Are you sure you want to delete ${gymSet.name} ${gymSet.reps}x${gymSet.weight}${gymSet.unit}?',),
            actions: <Widget>[
              ElevatedButton(
                child: const Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              ElevatedButton(
                child: const Text('Delete'),
                onPressed: () async {
                  final navigator = Navigator.of(context);
                  await db.gymSets.deleteOne(gymSet);
                  pagingController.refresh();
                  navigator.pop();
                },
              ),
            ],
          );
        },
      ),
    );
  }
}
