import 'package:fmassive/database.dart';
import 'package:moor/moor.dart';

Setting defaultSettings = Setting(
  alarm: true,
  vibrate: true,
  sound: null,
  notify: true,
  images: true,
  showUnit: true,
  lightColor: null,
  darkColor: null,
  steps: true,
  date: 'DD/MM/YYYY',
  showDate: true,
  theme: 'light',
  showSets: true,
  noSound: false,
  backup: false,
);

class Settings extends Table {
  BoolColumn get alarm => boolean()();
  BoolColumn get backup => boolean().nullable()();
  TextColumn get darkColor => text().nullable()();
  TextColumn get date => text().nullable()();
  BoolColumn get images => boolean().nullable()();
  TextColumn get lightColor => text().nullable()();
  BoolColumn get noSound => boolean().nullable()();
  BoolColumn get notify => boolean().nullable()();
  BoolColumn get showDate => boolean().nullable()();
  BoolColumn get showSets => boolean().nullable()();
  BoolColumn get showUnit => boolean().nullable()();
  TextColumn get sound => text().nullable()();
  BoolColumn get steps => boolean().nullable()();
  TextColumn get theme => text().nullable()();
  BoolColumn get vibrate => boolean()();
}
