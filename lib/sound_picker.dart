import 'package:audioplayers/audioplayers.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

class SoundPicker extends StatefulWidget {
  const SoundPicker({super.key, required this.onSelect, required this.path});

  final Function(String path) onSelect;
  final String? path;

  @override
  createState() => _SoundPickerState();
}

class _SoundPickerState extends State<SoundPicker> {
  final audioPlayer = AudioPlayer();

  @override
  void dispose() {
    audioPlayer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        FilePickerResult? result = await FilePicker.platform.pickFiles(
          type: FileType.audio,
        );
        if (result == null) return;
        final path = result.files.first.path;
        if (path == null) return;
        await audioPlayer.play(DeviceFileSource(path));
        widget.onSelect(path);
      },
      child: Text(widget.path != null
          ? "Sound: ${basename(widget.path!)}"
          : 'Alarm sound',),
    );
  }
}
